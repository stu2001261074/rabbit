public class StickerDecorator extends EggDecorator {
    public StickerDecorator(Egg decoratedEgg) {
        super(decoratedEgg);
    }

    @Override
    public void decorate() {
        super.decorate();
        System.out.println("Облепяне на яйцето!");
    }
}
