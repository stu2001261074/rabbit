import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class Basket extends Observable implements Observer {
    private List<Egg> eggs;
    private List<Observer> observers;

    public Basket() {
        eggs = new ArrayList<>();
        observers = new ArrayList<>();
    }

    public void addEgg(Egg egg) {
        eggs.add(egg);
        notifyObservers();
    }

    public List<Egg> getEggs() {
        return eggs;
    }

    public int getEggCount() {
        return eggs.size();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Observable) {
            System.out.println("Кокошето, пъдпъдъчето и динозавърското яйце са боядисани!");
        }
    }
    public void registerObserver(Observer observer) {
        addObserver(observer);
    }
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update(this, null);
        }
    }
}
