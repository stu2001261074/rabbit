public class EasterBunny {
    private static EasterBunny instance;

    private EasterBunny() {
    }

    public static EasterBunny getInstance() {
        if (instance == null) {
            instance = new EasterBunny();
        }
        return instance;
    }

    public void prepareEggs(EggFactory eggFactory, int count) {
        Basket basket = new Basket();

        for (int i = 0; i < count; i++) {
            Egg egg = eggFactory.createEgg();
            egg.decorate();
            basket.addEgg(egg);

            if (basket.getEggCount() > 5) {
                basket.notifyObservers();
            }
        }
    }
}
