import java.util.List;

public class Main {
    public static void main(String[] args) {
        EasterBunny bunny = EasterBunny.getInstance();

        EggFactory chickenEggFactory = new ChickenEggFactory();
        EggFactory quailEggFactory = new QuailEggFactory();
        EggFactory dinosaurEggFactory = new DinosaurEggFactory();

        Egg chickenEgg = chickenEggFactory.createEgg();
        Egg quailEgg = quailEggFactory.createEgg();
        Egg dinosaurEgg = dinosaurEggFactory.createEgg();

        Egg coloredChickenEgg = new ColoringDecorator(chickenEgg);
        Egg coloredQuailEgg = new ColoringDecorator(quailEgg);
        Egg coloredDinosaurEgg = new ColoringDecorator(dinosaurEgg);

        Egg decoratedChickenEgg = new StickerDecorator(coloredChickenEgg);
        Egg decoratedQuailEgg = new StickerDecorator(coloredQuailEgg);
        Egg decoratedDinosaurEgg = new StickerDecorator(coloredDinosaurEgg);

        Basket basket = new Basket();
        basket.registerObserver(new BunnyObserver());

        basket.addEgg(decoratedChickenEgg);
        basket.addEgg(decoratedQuailEgg);
        basket.addEgg(decoratedDinosaurEgg);

        List<Egg> eggs = basket.getEggs();
        int count = 5;

        bunny.prepareEggs(chickenEggFactory, count);
        bunny.prepareEggs(quailEggFactory, count);
        bunny.prepareEggs(dinosaurEggFactory, count);
    }
}
