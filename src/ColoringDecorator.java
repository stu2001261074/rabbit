public class ColoringDecorator extends EggDecorator {
    public ColoringDecorator(Egg decoratedEgg) {
        super(decoratedEgg);
    }

    @Override
    public void decorate() {
        super.decorate();
        System.out.println("Боядисване на яйцето!");
    }
}

