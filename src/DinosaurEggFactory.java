public class DinosaurEggFactory implements EggFactory {
    @Override
    public Egg createEgg() {
        return new DinosaurEgg();
    }
}
