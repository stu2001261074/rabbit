public class QuailEggFactory implements EggFactory {
    @Override
    public Egg createEgg() {
        return new QuailEgg();
    }
}
