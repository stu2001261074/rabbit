public class ChickenEggFactory implements EggFactory {
    @Override
    public Egg createEgg() {
        return new ChickenEgg();
    }
}
